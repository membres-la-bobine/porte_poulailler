Ce projet montre la réalisation d'une porte de poulailler de type 'guillotine' autmatique en utilisant un Arduino.

Matériel utilisé :
- Arduino UNO
- moteur pas à pas 28BYJ-48 5V avec une carte ULN2003
- photoresistance
- 2 capteurs de fin de course
- barre de 50cm (tuyau cuivre ici)
- transformateur 220V AC > 5V DC pour alimenter le moteur
- transformateur 220V AC > 9V DC pour alimenter l'Arduino UNO

Le moteur pas à pas est fixé grâce à un support imprimé en 3D (TODO AJOUTER FICHIER)
Le moteur est fixé à l'axe via un coupleur imprimé en 3D, le fichier openscad est disponible dans le dossier 3D/
#include <Stepper.h>


//#define DEBUG   // commenter cette ligne pour désactiver le debug
#ifdef DEBUG
  #define DPRINT(...)    Serial.print(__VA_ARGS__)
  #define DPRINTLN(...)  Serial.println(__VA_ARGS__)
#else
  #define DPRINT(...)
  #define DPRINTLN(...)
#endif

/* declaration des variables */
bool porteOuverte = false;
bool nuit = false;

const int PIN_FIN_COURSE_BASSE = 6;
const int PIN_FIN_COURSE_HAUTE = 7;
const int PIN_PHOTORESISTANCE = A0;

// delai en secondes avant d'actionner le moteur lors d'un changement de luminosité
const int DELAIS = 5;

// temps maximum d'activation du moteur lors d'une ouverture ou fermeture, multiplé par 10 ms
const int TEMPS_MAX_MOTEUR = 400; // 40 secondes

int decalage_ouverture = DELAIS; // décalage en secondes entre la détection d'un changement et le déclenchement du moteur
int decalage_fermeture = DELAIS;

//pour le capteur de fin de course, la valeur est LOW quand il ne connecte pas, HIGH quand il est touche.


// nombre de pas par rotation
const int pas_par_rotation = 400;
// see https://arduino.stackexchange.com/questions/57089/stepper-motor-wont-reverse-turn-ccw for the weird pin order !
Stepper moteur(pas_par_rotation, 8, 10, 9, 11);

void setup() { 
  // initialisation des broches entrees/sortie
  pinMode(PIN_FIN_COURSE_BASSE,INPUT_PULLUP);
  pinMode(PIN_FIN_COURSE_HAUTE, INPUT_PULLUP);
  pinMode(PIN_PHOTORESISTANCE, INPUT_PULLUP);

  // vitesse à 60 tours par minute
  moteur.setSpeed(60);

  Serial.begin(9600);

  ouverture();
}

void loop() {

  // lecture de la valeur de luminosité
  int valeur_luminosite = analogRead(A0);

  DPRINT("valeur luminosité :");
  DPRINTLN(valeur_luminosite);

  if (valeur_luminosite > 500) {
    nuit = true;
  } else {
    nuit = false;
  }

  if (nuit) {
    DPRINTLN("Nuit");
  } else {
    DPRINTLN("Jour");
  }

  bool course_haute = digitalRead(PIN_FIN_COURSE_HAUTE);
  bool course_basse = digitalRead(PIN_FIN_COURSE_BASSE);

  DPRINT("Course haute");
  DPRINTLN(course_haute);
  
  DPRINT("Course basse");
  DPRINTLN(course_basse);

  if (nuit) {
    decalage_ouverture = DELAIS;
    // si c'est la nuit
    if (porteOuverte) {
      if (decalage_fermeture <= 0) {
        decalage_fermeture = DELAIS;
        fermeture();
      } else {
        decalage_fermeture--;
      }
    }
  } else { // si c'est le jour
    decalage_fermeture = DELAIS;
    if (porteOuverte == false) {
      if (decalage_ouverture <= 0) {
        ouverture();
        decalage_ouverture = DELAIS;
      } else {
        decalage_ouverture--;
      }
    }
  }

  delay(1000);
}

void ouverture() {
  DPRINTLN("Ouverture de la porte");
  int max = TEMPS_MAX_MOTEUR;
  while (digitalRead(PIN_FIN_COURSE_HAUTE) == LOW and max >= 0) {
    moteur.step(-50);
    DPRINT(".");
    delay(10);
    max--;
  }

  powerOffStepper();
  
  porteOuverte = true;
  DPRINTLN("Porte ouverte");
}

void fermeture() {
  DPRINTLN("Fermeture de la porte");
  int max = TEMPS_MAX_MOTEUR;
  while (digitalRead(PIN_FIN_COURSE_BASSE) == LOW and max >= 0) {
    moteur.step(50);
    DPRINT(".");
    delay(10);
    max--;
  }

  powerOffStepper();
  
  porteOuverte = false;
  DPRINTLN("Porte fermée");
}

/*
 * Power down all stepper pins, so the stepper doesn't heat when not used.
 */
void powerOffStepper() {
  digitalWrite(8,LOW);
  digitalWrite(9,LOW);
  digitalWrite(10,LOW);
  digitalWrite(11,LOW);
}
